var http = require('http'),
  fileSystem = require('fs'),
  path = require('path');

//Lets define a port we want to listen to
const PORT=8080;

//We need a function which handles requests and send response
function handleRequest(request, response) {
  try {
    switch (request.url) {
      case '/':
        returnPage(response, '/index.html');
        break;
      default:
        returnPage(response, '/' + request.url);
        break;
    }
  }
  catch (error) {
      console.log("File not found");
  }
}

function returnPage(response, fileName) {
  try {
    var filePath = path.join(__dirname, fileName);
    var stat = fileSystem.statSync(filePath);

    response.writeHead(200, {
      'Content-Type': 'text/html',
      'Content-Length': stat.size
    });

    var readStream = fileSystem.createReadStream(filePath);
    // We replaced all the event handlers with a simple call to readStream.pipe()
    readStream.pipe(response);
  }
  catch (error) {
    console.log("File not found", error);
  }
}

//Create a server
var server = http.createServer(handleRequest);

//Lets start our server
server.listen(PORT, function(){
  //Callback triggered when server is successfully listening. Hurray!
  console.log("Server listening on: http://localhost:%s", PORT);
});